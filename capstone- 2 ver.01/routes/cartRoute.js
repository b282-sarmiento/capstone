

const express = require("express");
const router = express.Router();
const auth = require("../auth");
const cartController = require("../controllers/cartController"); // Correctly import the cartController

router.post("/addcart", auth.verify, (req, res) => {
  let data = {
    userId: auth.decode(req.headers.authorization).id,
    productId: req.body.productId,
  };
  cartController.addcart(data) // Use cartController instead of userController
    .then((resultFromController) => res.send(resultFromController))
    .catch((error) => {
      console.error('Error adding product to cart:', error);
      res.status(500).send({ success: false, message: 'An error occurred while adding the product to cart' });
    });
});

module.exports = router;