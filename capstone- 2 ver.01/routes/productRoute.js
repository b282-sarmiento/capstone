
const express = require("express");

const router = express.Router();

const auth = require ("../auth")

const productController = require("../controllers/productController")


router.post("/create", auth.verify, (req, res) => {
  const data = {
    product: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };

  productController.addProduct(data).then((resultFromController) =>
    res.send(resultFromController)
  );
});

router.get("/all", auth.verify, (req, res) => {
  const data = {
    products: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };
    productController.getAllProduct(data).then(resultFromController => res.send(resultFromController));  
  });


router.get("/active",(rec,res)=>{
    productController.gettAllActive().then(resultFromController =>res.send(resultFromController))
})
router.get("/:productId/details",(req, res)=>{
    productController.getProduct(req.params).then(resultFromController =>res.send(resultFromController))

})

router.get("/:productId", auth.verify, (req, res)=>{
    productController.updateProduct(req.params, req.body).then(resultFromController =>res.send(resultFromController))
})


router.put("/:_id", auth.verify, (req, res) => {
  const data = {
    products: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };

  productController.updateProduct1(req.params, data).then(resultFromController =>
    res.send(resultFromController)
  );
});


router.patch("/:productId/archive", auth.verify, (req, res) => {
  const data = {
    products: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };

  productController.archive(req.params, data).then(resultFromController =>
    res.send(resultFromController)
  );
});


router.patch("/:productId/activate", auth.verify, (req, res) => {
  const data = {
    products: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };

  productController.activate(req.params, data).then(resultFromController =>
    res.send(resultFromController)
  );
});



module.exports = router;