const express = require("express");
const auth = require("../auth");
const cart = require("../models/Cart");
const User = require("../models/Users");
const Product = require("../models/Product")




// Controller function to add a product to the user's cart
exports.addcart = async (data) => {
  try {
    const { userId, productId } = data;

    // First, check if the user exists in the database
    const user = await User.findById(userId);
    if (!user) {
      return { success: false, message: 'User not found' };
    }

    // If the user exists, add the product to their cart
    user.cart.push(productId);
    await user.save();

    return { success: true, message: 'Product added to cart successfully' };
  } catch (error) {
    console.error('Error adding product to cart:', error);
    return { success: false, message: 'An error occurred while adding the product to cart' };
  }
};