const express = require("express");
const auth = require("../auth");
const Product = require("../models/Product");


module.exports.addProduct = (data) => {
    
    if (data.isAdmin) {
        let newProduct = new Product({
            name : data.product.name,
            description : data.product.description,
            price : data.product.price
        });
        return newProduct.save().then((products, error) => {
            if (error) {
                return false;
            } else {
                return true;
            };
        });

    };
        let message = Promise.resolve("User must be Admin to access this.")
    return message.then((value) => {
        return {value}
    });
};



module.exports.getAllProduct = (data) => {
  if (data.isAdmin) {
    return Product.find({}).then((result, error) => {
      if (error) {
        return false;
      } else {
        return result;
      }
    });
  } else {
    let message = Promise.resolve("User must be Admin to access this.");
    return message.then((value) => {
      return { value };
    });
  }
};
    

module.exports.gettAllActive =()=>{
    return Product.find({isActive:true}).then(result =>{
        return result
    })
}

module.exports.getProduct =(reqParams) =>{
    return Product.findById(reqParams.productId).then(result =>{
        return result;
    })
};
module.exports.updateProduct =(reqParams) =>{
    return Product.findById(reqParams.productId).then(result =>{
        return result;
    })
};


module.exports.updateProduct1 = (productId, data) => {
  if (data.isAdmin) {
    let updateProduct = {
      name: data.products.name,
      description: data.products.description,
      price: data.products.price
    };

    return Product.findByIdAndUpdate(productId, updateProduct)
      .then(updatedProduct => {
        if (updatedProduct) {
          return true; // Product updated successfully
        } else {
          return false; // Product not found or not updated
        }
      })
      .catch(error => {
        console.error('Error updating product:', error);
        return false; // Error occurred during update
      });
  }
};


module.exports.archive = (reqParams, data) => {
  if (data.isAdmin) {
    let updateProduct = {
      isActive: false
    };

    return Product.findByIdAndUpdate(reqParams.productId, updateProduct)
      .then(product => {
        if (!product) {
          throw new Error("Product not found");
        }
        return true;
      })
      .catch((error) => {
        console.log(error);
        return ("you must be an admin");
      });
  }
};

module.exports.activate = (reqParams, data) => {
  if (data.isAdmin) {
    let updateProduct = {
      isActive: true
    };

    return Product.findByIdAndUpdate(reqParams.productId, updateProduct)
      .then(product => {
        if (!product) {
          throw new Error("Product not found");
        }
        return true;
      })
      .catch((error) => {
        console.log(error);
        return ("you must be an admin");
      });
  }
};
