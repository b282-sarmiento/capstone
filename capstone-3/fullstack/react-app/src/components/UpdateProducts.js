import React, { useState, useContext, useEffect } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

function ProductEdit(props) {
  const { _id } = useParams();
  const navigate = useNavigate();
  const user = useContext(UserContext);
  const token = localStorage.getItem('token'); // Assuming you have a token in your localStorage
  const [product, setProduct] = useState({
    name: '',
    description: '',
    price: 0,
  });

  useEffect(() => {
    // Fetch product data here if needed
    // For example:
    fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setProduct({
          name: data.name,
          description: data.description,
          price: data.price,
        });
      });
  }, [_id, token]);

  const updateProduct = (e) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        name: product.name,
        description: product.description,
        price: product.price,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setProduct({
            name: '',
            description: '',
            price: 0,
          });

          Swal.fire({
            title: 'Success!',
            icon: 'success',
            text: 'Product Updated!',
          });

          navigate('/adminDash');
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please, try again.',
          });
        }
      });
  };

  return (
    <Container>
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="text-center">
              <Form>
                <Form.Group controlId="formName">
                  <Form.Label>Name</Form.Label>
                  <Form.Control
                    type="text"
                    value={product.name}
                    onChange={(e) =>
                      setProduct({ ...product, name: e.target.value })
                    }
                    placeholder="Product Name"
                  />
                </Form.Group>
                <Form.Group controlId="formDescription">
                  <Form.Label>Description</Form.Label>
                  <Form.Control
                    as="textarea"
                    value={product.description}
                    onChange={(e) =>
                      setProduct({ ...product, description: e.target.value })
                    }
                    placeholder="Product Description"
                  />
                </Form.Group>
                <Form.Group controlId="formPrice">
                  <Form.Label>Price</Form.Label>
                  <Form.Control
                    type="number"
                    value={product.price}
                    onChange={(e) =>
                      setProduct({ ...product, price: e.target.value })
                    }
                    placeholder="Product Price"
                  />
                </Form.Group>
              </Form>
              {user.id !== null ? (
                <Button variant="primary" onClick={updateProduct}>
                  Update Product
                </Button>
              ) : (
                <Button className="btn btn-danger" as={Link} to="/login">
                  Log in to Update
                </Button>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default ProductEdit;