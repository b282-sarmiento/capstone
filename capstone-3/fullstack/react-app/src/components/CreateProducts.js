import React, { useState, useContext } from 'react';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

function CreateProducts(props) {
  const { user } = useContext(UserContext);
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);

  const create = () => {
    const productData = {
      name: name,
      description: description,
      price: price,
    };

    fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(productData),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: 'Success',
            icon: 'success',
            text: 'You have successfully created the product.',
          });
        } else {
          Swal.fire({
            title: 'Error',
            icon: 'error',
            text: 'An error occurred while creating the product. Please try again.',
          });
        }
      })
  };

  return (
    <div>
      <form>
        <input
          type="text"
          value={name}
          onChange={(e) => setName(e.target.value)}
          placeholder="Product Name"
        />
        <textarea
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          placeholder="Product Description"
        />
        <input
          type="number"
          value={price}
          onChange={(e) => setPrice(e.target.value)}
          placeholder="Product Price"
        />
      </form>
      <button onClick={create}>Create Product</button>
    </div>
  );
}

export default CreateProducts;