import React, { useContext } from 'react';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import UserContext from '../UserContext';
import { Link, NavLink } from 'react-router-dom';

function AppNavbar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar expand="lg" className="bg-body-tertiary">
      <Navbar.Brand as={Link} to="/">ALBION-MARKET</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="me-auto">
          <Nav.Link as={NavLink} to="/">Home</Nav.Link>

          {/* Render "Products" link based on user isAdmin status */}
          {user.isAdmin ? (
            <>
              <Nav.Link as={NavLink} to="/adminDash">Admin Dashboard</Nav.Link>
              <Nav.Link as={NavLink} to="/ReactDeact">ReactDeact</Nav.Link>
              <Nav.Link as={NavLink} to="/createproducts">Create Product</Nav.Link>
            </>
          ) : (
            <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
          )}

          {/* Render "Logout" link if user is authenticated, otherwise render "Register" and "Login" links */}
          {user.id ? (
            <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
          ) : (
            <>
              <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
              <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
            </>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default AppNavbar;