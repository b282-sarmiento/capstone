import {useState, useEffect} from 'react';

import { Button, Row, Col, Card } from 'react-bootstrap';

import {Link} from 'react-router-dom';




export default function ProductCardAdmin({products}) {
  const { name, description, price, _id } = products;
return (
 

    <Row className="mt-3 mb-3">
        <Col xs={12}>
            <Card className="cardHighlight p-0">
                <Card.Body>
                    <Card.Title><h4>{name}</h4></Card.Title>
                    <Card.Subtitle>Description</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price</Card.Subtitle>
                    <Card.Text>{price}</Card.Text>
                    <Button className="bg-primary" as={Link} to={`/adminDash/${_id}`}>update</Button>
                </Card.Body>
            </Card>
        </Col>
    </Row>       
    )
}
