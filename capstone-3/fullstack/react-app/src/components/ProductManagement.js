import React, { useState, useEffect } from 'react';
import Swal from 'sweetalert2';
import { useParams, useNavigate } from 'react-router-dom';

function ProductManagement() {
  const navigate = useNavigate();
  const token = localStorage.getItem('token'); // Replace with your token retrieval logic

  const { productId } = useParams();
  const [isActive, setIsActive] = useState(false); // Initialize with false

  useEffect(() => {
    // Fetch the product's isActive status
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setIsActive(data.isActive);
      });
  }, [productId, token]);

  const toggleProductStatus = () => {
    setIsActive((prevStatus) => !prevStatus);
  };

  const updateProductStatus = (e) => {
    e.preventDefault();

    const action = isActive ? 'archive' : 'activate';

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/${action}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        isActive: !isActive, // Invert the status
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          Swal.fire({
            title: 'Success!',
            icon: 'success',
            text: `Product ${action}d successfully!`,
          });

          // Redirect back to the admin dashboard
          navigate('/adminDash');
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please try again.',
          });
        }
      });
  };

  return (
    <div>
      <h2>Toggle Product Status</h2>
      <form onSubmit={updateProductStatus}>
        <div>
          <label>Status: {isActive ? 'active' : 'archived'}</label>
          <button type="submit">
            {isActive ? 'Archive Product' : 'Activate Product'}
          </button>
        </div>
      </form>
    </div>
  );
}

export default ProductManagement;