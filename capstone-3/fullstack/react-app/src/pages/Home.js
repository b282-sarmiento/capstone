import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

	const data = {
		title: "Albion Market",
		content: "You can buy Any equipment Anywhere you want.",
		destination: "/products",
		label: "Buy now!"
	}


	return (
		<>
		<Banner data={data} />
    	<Highlights />
    	
		</>
	)
}


