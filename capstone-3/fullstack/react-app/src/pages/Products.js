// import coursesData from '../data/coursesData';
import ProductCard from '../components/ProductCard';

import {useState, useEffect} from 'react';

export default function Products() {

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(res => res.json())
		.then(data => {
			setProducts(data.map(products => {
				return (
					<ProductCard key={products.id} products = {products} />
				)
			}))
		})
	}, []);
	return (
		<>
		{products}
		</>
	)
};
