import React, { useState, useEffect } from 'react';
import ProductCardAdmin from '../components/ProductCardAdmin';
// ... (other imports and code)

const AdminDash = () => {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    // Define a function to fetch data and handle errors
    const fetchData = async () => {
      try {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`, 
          },
        });
        
        if (!response.ok) {
          throw new Error('Failed to fetch data');
        }
        
        const data = await response.json();
        console.log(data); // Add this line to debug the content of 'data'
        
        setProducts(data.map(products => (
          <ProductCardAdmin key={products.id} products={products} />
        )));
      } catch (error) {
        // Handle any errors that occurred during the fetch
        console.error('Error fetching data:', error);
      }
    };

    fetchData(); // Call the fetchData function to trigger the fetch when the component mounts
  }, []);

  return (
    <div> {/* Add a wrapping div */}
      {products}
    </div>
  );
};

export default AdminDash;